#!/usr/bin/python3

import cv2
import numpy as np
import socket
import time

from PIL import Image

header = '''
  ______    _________    ___   ____   ____  ________  
.' ____ \  |  _   _  | .'   `.|_  _| |_  _||_   __  | 
| (___ \_| |_/ | | \_|/  .-.  \ \ \   / /    | |_ \_| 
 _.____`.      | |    | |   | |  \ \ / /     |  _| _  
| \____) | _  _| |_  _\  `-'  /_  \ ' /_    _| |__/ | 
 \______.'(_)|_____|(_)`.___.'(_)  \_/(_)  |________| 
				 STream OVer Ethernet
'''

SUBFOLDER = "./Images/"
close = b'3'
TCP_IP = '192.168.1.142'
TCP_PORT = 1337

def extract():
	vidcap = cv2.VideoCapture("Touhou Bad Apple!!.mp4")
	succes,image = vidcap.read()
	count = 0
	success = True
	
	while succes:
		success,image = vidcap.read()
		print("Read a new frame: ", success)
		cv2.imwrite(SUBFOLDER+"Frame%d.jpg" % count, image)
		count += 1

def blackWhiteConverter():
	for i in range(0, 6565):
		col = Image.open(SUBFOLDER+"Frame"+str(i)+".jpg")
		gray = col.convert("L")
		bw = gray.point(lambda x: 0 if x<128 else 255, "1")
		bw.save(SUBFOLDER+"greyFrame"+str(i)+".png")
		
def resize():
	for i in range(0, 6564):
		img = cv2.imread(SUBFOLDER+"greyFrame"+str(i)+".png")
		newImg = cv2.resize(img, (128,64))
		cv2.imwrite(SUBFOLDER+"resizedFrame"+str(i)+".png")
		
def convertFrameToRaw():
	for i in range(0, 6563):
		destBuffer = np.full([128,64], 0)
		im = Image.open(SUBFOLDER+"resizedFrame"+str(i)+".png")
		pix = im.load()
		for y in range(0,64):
			for x in range(0,128):
				temp, temp1, temp2 = pix[x,y]
				if(temp > 128):
					destBuffer[x,y] = 1
				else:
					destBuffer[x,y] = 0
	
		np.savetxt(SUBFOLDER+'rawFrame'+str(i)+'.txt', destBuffer, fmt='%d', delimiter=',')
		
def rleCompressor(source):
	width, height = source.shape
	tempDestination = []
	destination = b""
	trigger = 0
	count = 1
	lst = []
	prev = source[0,0]
	for y in range(0, height):
		for x in range(0, width):
			#print(x, " ", count)
			if(source[x,y] != prev or trigger == 1):
				#print("Triggerd on", count)
				if (x == 0):
					start = (128-(count-1)).to_bytes(1, byteorder='big')
					row = (y-1).to_bytes(1, byteorder='big')
				else:
					start = (x+1-count).to_bytes(1, byteorder='big')
					row = (y).to_bytes(1, byteorder='big')
				amount = (count).to_bytes(1, byteorder='big')
				destinationCharacter = (source[x,y].item()).to_bytes(1, byteorder='big')


				entry = [row, start, amount, prev.astype(int)]
				lst.append(entry)
				count = 1
				trigger = 0
				prev = source[x,y]
			else:
				count += 1
				if (x == 127):
					trigger = 1

	if(int.from_bytes(lst[0][2], byteorder='big') > 128):
		temp = int.from_bytes(lst[0][2], byteorder='big') - 1
		lst[0][2] = temp.to_bytes(1, byteorder='big')

	return lst


def stream():
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect((TCP_IP, TCP_PORT))
	counter = 1
	print("connected")
	
	for i in range(321, 6563):
		tempBuffer = b""
		destBuffer = np.loadtxt(SUBFOLDER+'rawFrame'+str(i)+'.txt', delimiter=',')
		destBuffer = destBuffer.astype(int)
		tempBuffer = rleCompressor(destBuffer)
		print("Writing frame:", counter)
		for p in tempBuffer:
			recvBuffer = []
			s.send(b''.join(p))
			recvBuffer = s.recv(1)
			while(recvBuffer[0] != 0x32):
				time.sleep(1)
	
		counter += 1
	
	s.send(close)
	s.close()
	
if __name__ == '__main__':
	while(True):
		print(header)
		print ("""
		1.Extract from video
		2.Convert to black and white
		3.Resize frames
		4.Convert frames to raw files
		5.Stream video
		""")
		ans=input("Command: ") 
		if ans=="1": 
			extract()
			print("\n Frames extracted") 
		elif ans=="2":
			blackWhiteConverter()
			print("\n Frames converted to black and white") 
		elif ans=="3":
			resize()
			print("\n Frames resized") 
		elif ans=="4":
			convertFrameToRaw()
			print("\n Frames converted to raw files") 
		elif ans !="":
			stream()
			print("\n Stream ended") 
