#############################################################################
#
# Project Makefile
#
# (c) Wouter van Ooijen (www.voti.nl) 2016
#
# This file is in the public domain.
# 
#############################################################################

# source files in this project (main.cpp is automatically assumed)
SOURCES := EthernetShield.cpp TCPServer.cpp TCPClient.cpp

# header files in this project
HEADERS := due-spi.hpp register_map.hpp EthernetShield.hpp TCPServer.hpp TCPClient.hpp

# other places to look for files for this project
SEARCH  := 

# set RELATIVE to the next higher directory 
# and defer to the appropriate Makefile.* there
RELATIVE := ..
include $(RELATIVE)/Makefile.due
